#!/bin/bash

usage="
Usage: fiab-setup.sh <fiab branch> <postgres version>

 Where fiab-branch like rpi_dev or rpi and postgres version is like 13 to 16.

"

# show help if count of params is zero
if [ $# -lt 2 ]; then
    echo "$usage"
    exit
fi

wget --spider --quiet https://google.com
if [ "$?" != 0 ]; then
    echo "No internet connection" 
    echo "Not running script"
else 

    echo  "\e[92m Update and upgrade Raspbian Lite\e[0m"
    sudo apt update -y && sudo apt upgrade -y
    sudo apt install -y git
    sudo apt autoremove -y
    echo  "\e[92m Checking for Docker...\e[0m"

    if hash docker 2>/dev/null; then
        echo  "\e[92m Docker already installed, skipping\e[0m"
    else
        echo  "\e[92m Installing Docker from github script\e[0m"
        curl -fsSL https://get.docker.com -o get-docker.sh
        sed -i 's/9)/10)/' get-docker.sh
        sudo sh ./get-docker.sh
        sudo usermod -aG docker $USER
    fi
    echo  "\e[92m Checking for Docker-compose...\e[0m"

    if hash docker-compose 2>/dev/null; then
        echo  "\e[92m Docker-compose already installed, skipping\e[0m"
    else
        echo  "\e[92m Installing Docker-compose prerequisites and docker-compose from python wheel\e[0m"
        # updated to python 3 for docker-compose install
	sudo apt install -y python3 python3-pip libffi-dev libssl-dev
        sudo apt-get remove python-configparser
        sudo pip3 install docker-compose
    fi

   

    DOCKER_COMPOSE_DIR="$HOME/fiab"
    EXAMPLES_DIR="$HOME/examples"
    DOCKER_CONFIG_FILE="/etc/docker/daemon.json"

    if [ -f "$DOCKER_CONFIG_FILE" ]; then
        echo "\e[92m $DOCKER_CONFIG_FILE exists, skipping.\e[0m"
    else 
        echo "\e[92m $DOCKER_CONFIG_FILE does not exist, copying config file...\e[0m"
        sudo cp daemon.json $DOCKER_CONFIG_FILE
    fi

     echo  "\e[92m Updating ~/fiab/docker-compose.yml...\e[0m"

    if [ -d "$DOCKER_COMPOSE_DIR" ]; then
        echo  "\e[92m Directory detected, pulling newest updates\e[0m"
        echo  "\e[92m Any local changes will be lost!\e[0m"
        cd "$DOCKER_COMPOSE_DIR"
        git fetch --all
        git reset --hard origin/master
    else
        echo  "\e[92m No Prexisting directories detected, cloning docker compose\e[0m"
        mkdir -p "$DOCKER_COMPOSE_DIR"
        git clone https://dowster64@bitbucket.org/suttontools/docker-compose.git "$DOCKER_COMPOSE_DIR"
    fi

    if [ -d "$EXAMPLES_DIR" ]; then
        echo  "\e[92m Directory detected, pulling newest updates\e[0m"
        echo  "\e[92m Any local changes will be lost!\e[0m"

        cd "$EXAMPLES_DIR"
        git fetch --all
        git reset --hard origin/master
    else
        echo  "\e[92m No Prexisting directories detected, cloning docker compose\e[0m"
        mkdir -p "$EXAMPLES_DIR"
        git clone https://dowster64@bitbucket.org/suttontools/fiab-node-red-examples.git "$EXAMPLES_DIR"
    fi

    echo  "\e[92m Running Docker containers: ~/fiab/docker-compose.yml -d\e[0m"
    echo  "\e[92m This may take a while if it is the fist time...\e[0m"

    cd "$DOCKER_COMPOSE_DIR"

    if command docker-compose ps --quiet>/dev/null; then
        docker-compose down
        docker-compose up -d
    else
        docker-compose up -d
    fi
    echo "\e[92m Creating Symlinks for docker volumes...\e[0m"
    
    cd ~
    sudo chmod a+x /var/lib/docker/volumes
    
    if [ ! -L ~/postgres-data ]
    then
        echo "\e[92m Creating postgres-data symlink\e[0m"
        ln -sf /var/lib/docker/volumes/fiab_postgres-data/_data ~/postgres-data
    else
        echo "\e[92m~/postgres-data symlink already exists...\e[0m"
    fi
    
    if [ ! -L ~/pgadmin-data ]
    then
        echo "\e[92m Creating pgadmin-data symlink\e[0m"
        ln -sf /var/lib/docker/volumes/fiab_pgadmin-data/_data ~/pgadmin-data
    else
        echo "\e[92m~/pgadmin-data symlink already exists...\e[0m"
    fi
    
    if [ ! -L ~/nodered-data ]
    then
        echo "\e[92m Creating nodered-data symlink\e[0m"
        ln -sf /var/lib/docker/volumes/fiab_nodered-data/_data ~/nodered-data
    else
        echo "\e[92m~/nodered-data symlink already exists...\e[0m"
    fi
    
    if [ ! -L ~/mosquitto-data ]
    then
        echo "\e[92m Creating mosquitto-data symlink\e[0m"
        ln -sf /var/lib/docker/volumes/fiab_mosquitto-data/_data ~/mosquitto-data
    else
        echo "\e[92m~/mosquitto-data symlink already exists...\e[0m"
    fi
    
    echo "\e[92m all done...\e[0m"
fi
