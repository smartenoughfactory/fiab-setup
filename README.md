# fiab-setup

*This script installs and modifies software on your RPi and runs some commands as root in order to create the Factory in a Box Docker environment.
This has been tested on a RPi 4 with 4GB of memory.*

***It requires an internet connection.***

*You can [find out about the Factory in a box here](https://smartenoughfactory.atlassian.net/wiki/spaces/SEF/overview) for a full description of the project.*

---
## Latest Information 

### Node-RED
Current version 1.2.5. Global packages installed:

* node-red-contrib-xbee.
* echarts.

#### settings.js
This can be now easily modified, Included are static js library and css files for echarts and w2ui.  The httpStatic variable is also set for local access to these libraries for connected clients. 

### PostgreSQL
This is fixed at the latest update of version 11.  This version is chosen because node-red-contrib-re-postgres is not compatable with vesion 12. 

### PgAdmin
Current version is 4.28.  DB Tools (backup and restore) are now available.  Later versions also enables packages

---
## New Features

* Symlinks in user directory to more easily access docker volumes.
* Node-RED settings.js is now exposed outside the container.
	* This means that passwords can be set.
	* security improved.
	* httpStatic defined. 
* DB backup and restore via PgAdmin.
* Postgres settings exposed.
* echarts available through installed npm package rather than via js script in html header.


## Prerequisite nodes

1. node-red-contrib-xbee (installed) if [the default image](https://hub.docker.com/r/suttontools/fiab-nodered) is used.  Otherwise install it.  
2. node-red-contrib-re-postgres.
3. node-red-dashboard.

---
SJD 26/11/2020

